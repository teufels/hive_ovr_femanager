<?php

declare(strict_types=1);

return [
    \HIVE\HiveOvrFemanager\Domain\Model\User::class => [
        'tableName' => 'fe_users',
        'properties' => [
            'department' => [
                'fieldName' => 'department'
            ],
            'twitterProfile' => [
                'fieldName' => 'twitter_profile'
            ],
            'facebookProfile' => [
                'fieldName' => 'facebook_profile'
            ],
            'instagramProfile' => [
                'fieldName' => 'instagram_profile'
            ],
            'linkedinProfile' => [
                'fieldName' => 'linkedin_profile'
            ],
            'skypeId' => [
                'fieldName' => 'skype_id'
            ],
            'aboutMe' => [
                'fieldName' => 'about_me'
            ],
        ],
    ],
];