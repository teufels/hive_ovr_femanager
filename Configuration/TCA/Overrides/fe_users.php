<?php
defined('TYPO3_MODE') or die();

/**
 * Add new fields to fe_users table
 */
$GLOBALS['TCA']['fe_users']['ctrl']['type'] = 'tx_extbase_type';

$tmp_fe_users_columns = array(
    'department' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:hive_ovr_femanager/Resources/Private/Language/locallang_db.xlf:tx_hiveovrfemanager_domain_model_user.department',
        'config' => array(
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ),
    ),
    'twitter_profile' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:hive_ovr_femanager/Resources/Private/Language/locallang_db.xlf:tx_hiveovrfemanager_domain_model_user.twitter_profile',
        'config' => array(
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ),
    ),
    'facebook_profile' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:hive_ovr_femanager/Resources/Private/Language/locallang_db.xlf:tx_hiveovrfemanager_domain_model_user.facebook_profile',
        'config' => array(
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ),
    ),
    'instagram_profile' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:hive_ovr_femanager/Resources/Private/Language/locallang_db.xlf:tx_hiveovrfemanager_domain_model_user.instagram_profile',
        'config' => array(
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ),
    ),
    'linkedin_profile' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:hive_ovr_femanager/Resources/Private/Language/locallang_db.xlf:tx_hiveovrfemanager_domain_model_user.linkedin_profile',
        'config' => array(
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ),
    ),
    'skype_id' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:hive_ovr_femanager/Resources/Private/Language/locallang_db.xlf:tx_hiveovrfemanager_domain_model_user.skype_id',
        'config' => array(
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ),
    ),
    'about_me' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:hive_ovr_femanager/Resources/Private/Language/locallang_db.xlf:tx_hiveovrfemanager_domain_model_user.about_me',
        'config' => array(
            'type' => 'text',
            'size' => 30,
            'rows' => 5,
            'enableRichtext' => true,
        ),
    ),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tmp_fe_users_columns,1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'fe_users',
    '
        --div--;Social Profiles, twitter_profile, facebook_profile, instagram_profile, linkedin_profile, skype_id
    '
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'fe_users',
    'department',
    '',
    'after:company'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'fe_users',
    'about_me',
    '',
    'after:image'
);