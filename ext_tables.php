<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

/**
 * Add static File Template
 */
call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ovr_femanager', 'Configuration/TypoScript', 'hive_ovr_femanager');

    }
);

/**
 * Add user TSConfig
 */
$_EXTKEY ='hive_ovr_femanager';
$extKey = $_EXTKEY;
call_user_func(
    function($extkey)
    {
        /********************
         * Add user TSConfig
         ********************/
        $userTsConfig = \TYPO3\CMS\Core\Utility\GeneralUtility::getUrl(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($extkey) . 'Configuration/TsConfig/User/config.txt');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig($userTsConfig);
    },
    $_EXTKEY
);


/**
 * Add page TSConfig for the new fields
 */
$tsConfig  = '';
$tsConfig .= 'tx_femanager.flexForm.new.addFieldOptions.department = Department' . PHP_EOL;
$tsConfig .= 'tx_femanager.flexForm.new.addFieldOptions.twitterProfile = Twitter Profile' . PHP_EOL;
$tsConfig .= 'tx_femanager.flexForm.new.addFieldOptions.facebookProfile = Facebook Profile' . PHP_EOL;
$tsConfig .= 'tx_femanager.flexForm.new.addFieldOptions.instagramProfile = Instagram Profile' . PHP_EOL;
$tsConfig .= 'tx_femanager.flexForm.new.addFieldOptions.linkedinProfile = LinkedIn Profile' . PHP_EOL;
$tsConfig .= 'tx_femanager.flexForm.new.addFieldOptions.skypeId = Skype ID' . PHP_EOL;
$tsConfig .= 'tx_femanager.flexForm.new.addFieldOptions.aboutMe = About me' . PHP_EOL;
$tsConfig .= 'tx_femanager.flexForm.edit < tx_femanager.flexForm.new';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig($tsConfig);


