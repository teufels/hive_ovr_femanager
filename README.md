![VEB](https://img.shields.io/badge/VEB-Virtual_Event_Base-FBD75E.svg)
![VENDOR](https://img.shields.io/badge/vendor-HIVE-219A83.svg)
![KEY](https://img.shields.io/badge/key-hive__ovr__femanager-blue.svg)
![version](https://img.shields.io/badge/version-0.0.2-yellow.svg?style=flat-square)
![depends](https://img.shields.io/badge/depends-in2code/femanager-red.svg?style=flat-square)

# addon extension to TYPO3 extension femanager

## Changelog
- 1.0.2 Add "About me" to user fields
- 1.0.1 Add Twitter, Facebook, Instagram LinkedIn Profiles & Skype ID to user fields
- 1.0.0 Build Extension to expand fe_user & femanager

## ToDo:
- Add Twitter, Instagram, Facebook Profiles to user fields

## Documentation
https://docs.typo3.org/p/in2code/femanager/master/en-us/Features/NewFields/Index.html