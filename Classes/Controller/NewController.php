<?php
namespace HIVE\HiveOvrFemanager\Controller;

class NewController extends \In2code\Femanager\Controller\NewController {

    /**
     * action create
     *
     * @param HIVE\HiveOvrFemanager\Domain\Model\User $user
     * @validate $user In2code\Femanager\Domain\Validator\ServersideValidator
     * @validate $user In2code\Femanager\Domain\Validator\PasswordValidator
     * @return void
     */
    public function createAction(\HIVE\HiveOvrFemanager\Domain\Model\User $user) {
        parent::createAction($user);
    }
}