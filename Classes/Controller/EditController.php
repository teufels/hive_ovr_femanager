<?php
namespace HIVE\HiveOvrFemanager\Controller;

class EditController extends \In2code\Femanager\Controller\EditController {

//    should fix not working keep password if empty error (https://github.com/in2code-de/femanager/issues/76)
//    /**
//     * @return void
//     */
//    public function initializeUpdateAction()
//    {
//        parent::initializeUpdateAction();
//    }


    /**
     * action update
     *
     * @param HIVE\HiveOvrFemanager\Domain\Model\User $user
     * @validate $user In2code\Femanager\Domain\Validator\ServersideValidator
     * @validate $user In2code\Femanager\Domain\Validator\PasswordValidator
     * @return void
     */
    public function updateAction(\HIVE\HiveOvrFemanager\Domain\Model\User $user) {
        parent::updateAction($user);
    }
}
