<?php

namespace HIVE\HiveOvrFemanager\Task;


use In2code\Femanager\Utility\HashUtility;
use In2code\Femanager\Utility\StringUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

/**
 * Class SendConfirmationTask.
 */
class SendConfirmationTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask
{

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var ConfigurationManagerInterface
     */
    protected $configurationManager;

    /**
     * TypoScript
     *
     * @var array
     */
    public $config;


    public function execute() {
        $success = true;


        $this->objectManager = GeneralUtility::makeInstance(
            \TYPO3\CMS\Extbase\Object\ObjectManager::class
        );

        $this->configurationManager = GeneralUtility::makeInstance(
            'TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager'
        );
        $extbaseFrameworkConfiguration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
        $this->config = $extbaseFrameworkConfiguration['plugin.']['tx_femanager.']['settings.'];


        $userBackendController = $this->objectManager->get(
            \In2code\Femanager\Controller\UserBackendController::class
        );


        $userRepository = $this->objectManager->get(
            \In2code\Femanager\Domain\Repository\UserRepository::class
        );

        $userBackendController->config = $this->config;

        //find unconfirmed
        $filter = [];
        $users = $userRepository->findAllInBackendForConfirmation($filter, false);

        foreach ($users as $user) {
            $userBackendController->sendCreateUserConfirmationMail($user);
        }

        $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface::class)->persistAll();

        return $success;
    }

}