<?php declare(strict_types = 1);

namespace HIVE\HiveOvrFemanager\Hooks;

/**
 * Hook which replaces String MARKER in (Text)CEs
 */
class ContentPostProc
{

    /**
     * @var array
     */
    var $simpleSearchReplacements;

    function __construct() {

        /**
         * Just add your search/replace items.
         * Each item will be replaced in the output.
         */
        $this->simpleSearchReplacements = [
            '###USER_FIRSTNAME###' => (string)$GLOBALS['TSFE']->fe_user->user['first_name'],
            '###USER_LASTNAME###' => (string)$GLOBALS['TSFE']->fe_user->user['last_name'],
            '###USER_NAME###' => (string)$this->getUserFullname(),
            '###USER_USERNAME###' => (string)$GLOBALS['TSFE']->fe_user->user['username'],
        ];
    }

    /**
     * @param array $parameters
     */
    public function run(array &$parameters)
    {
        $parameters['pObj']->content = $this->simpleReplacements($parameters['pObj']->content);
    }

    /**
     * Simple string replacements
     * if $this->simpleSearchReplacements has replacements
     *
     * @param $searchText
     * @return string
     */
    protected function simpleReplacements($searchText): string
    {
        if (!empty($this->simpleSearchReplacements)) {
            return str_replace(array_keys($this->simpleSearchReplacements), array_values($this->simpleSearchReplacements), $searchText);
        }

        return $searchText;
    }

    /**
     * @return mixed|string|null
     */
    protected function getUserFullname()
    {

        $user = $GLOBALS['TSFE']->fe_user->user;

        if ($user) {
            if ($user['first_name'] && $user['last_name']) {
                $content = $user['first_name'] . ' ' . $user['last_name'];
            } else if ($user['last_name']) {
                $content = $user['last_name'];
            } else if ($user['first_name']) {
                $content = $user['first_name'];
            } else {
                $content = "";
            }
            return $content;
        }
        return null;
    }

}