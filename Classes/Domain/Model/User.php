<?php
namespace HIVE\HiveOvrFemanager\Domain\Model;

class User extends \In2code\Femanager\Domain\Model\User {

    /**
     * department
     *
     * @var string
     */
    protected $department;

    /**
     * twitterProfile
     *
     * @var string
     */
    protected $twitterProfile;

    /**
     * facebookProfile
     *
     * @var string
     */
    protected $facebookProfile;

    /**
     * instagramProfile
     *
     * @var string
     */
    protected $instagramProfile;

    /**
     * linkedinProfile
     *
     * @var string
     */
    protected $linkedinProfile;

    /**
     * skypeId
     *
     * @var string
     */
    protected $skypeId;


    /**
     * aboutMe
     *
     * @var string
     */
    protected $aboutMe;

    /**
     * Returns the department
     *
     * @return string $department
     */
    public function getDepartment() {
        return $this->department;
    }

    /**
     * Sets the department
     *
     * @param string $department
     * @return void
     */
    public function setDepartment($department) {
        $this->department = $department;
    }

    /**
     * Returns the twitterProfile
     *
     * @return string $twitterProfile
     */
    public function getTwitterProfile() {
        return $this->twitterProfile;
    }

    /**
     * Sets the twitterProfile
     *
     * @param string $twitterProfile
     * @return void
     */
    public function setTwitterProfile($twitterProfile) {
        $this->twitterProfile = $twitterProfile;
    }

    /**
     * Returns the facebookProfile
     *
     * @return string $facebookProfile
     */
    public function getFacebookProfile() {
        return $this->facebookProfile;
    }

    /**
     * Sets the  facebookProfile
     *
     * @param string $facebookProfile
     * @return void
     */
    public function setFacebookProfile($facebookProfile) {
        $this->facebookProfile = $facebookProfile;
    }

    /**
     * Returns the instagramProfile
     *
     * @return string $instagramProfile
     */
    public function getInstagramProfile() {
        return $this->instagramProfile;
    }

    /**
     * Sets the instagramProfile
     *
     * @param string $instagramProfile
     * @return void
     */
    public function setInstagramProfile($instagramProfile) {
        $this->instagramProfile = $instagramProfile;
    }

    /**
     * Returns the linkedinProfile
     *
     * @return string $linkedinProfile
     */
    public function getLinkedinProfile() {
        return $this->linkedinProfile;
    }

    /**
     * Sets the linkedinProfile
     *
     * @param string $linkedinProfile
     * @return void
     */
    public function setLinkedinProfile($linkedinProfile) {
        $this->linkedinProfile = $linkedinProfile;
    }

    /**
     * Returns the skypeId
     *
     * @return string $skypeId
     */
    public function getSkypeId() {
        return $this->skypeId;
    }

    /**
     * Sets the skypeId
     *
     * @param string $skypeId
     * @return void
     */
    public function setSkypeId($skypeId) {
        $this->skypeId = $skypeId;
    }

    /**
     * Returns the aboutMe
     *
     * @return string $aboutMe
     */
    public function getAboutMe() {
        return $this->aboutMe;
    }

    /**
     * Sets the aboutMe
     *
     * @param string $aboutMe
     * @return void
     */
    public function setAboutMe($aboutMe) {
        $this->aboutMe = $aboutMe;
    }

    /**
     * @param string $username
     */
    public function setUsername($username) {
        $this->username = $username;
    }

}
