<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "hive_ovr_femanager"
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'hive_ovr_femanager',
    'description' => 'Extend fe_users and femanager for VEB',
    'category' => 'plugin',
    'author' => 'Bastian Holzem',
    'author_email' => 'b.holzem@teufels.com',
    'author_company' => 'teufels',
    'shy' => '',
    'priority' => '',
    'module' => '',
    'state' => 'beta',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'lockType' => '',
    'version' => '0.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '>=10.4.0',
            'femanager' => '>=6.0.0'
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
];
