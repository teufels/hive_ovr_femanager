CREATE TABLE fe_users (
    department varchar(255) DEFAULT '' NOT NULL,
	twitter_profile varchar(255) DEFAULT '' NOT NULL,
    facebook_profile varchar(255) DEFAULT '' NOT NULL,
    instagram_profile varchar(255) DEFAULT '' NOT NULL,
    linkedin_profile varchar(255) DEFAULT '' NOT NULL,
	skype_id varchar(255) DEFAULT '' NOT NULL,
    about_me text,
    tx_extbase_type varchar(255) DEFAULT '0' NOT NULL,
);