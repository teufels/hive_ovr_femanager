<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

call_user_func(function () {

    // Register extended domain class
    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
        ->registerImplementation(
            \In2code\Femanager\Domain\Model\User::class,
            \HIVE\HiveOvrFemanager\Domain\Model\User::class
        );

    // Register extended service class -> not needed yet because bug could not be resolved by override
    /*
    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
        ->registerImplementation(
            \In2code\Femanager\Domain\Service\SendMailService::class,
            \HIVE\HiveOvrFemanager\Domain\Service\SendMailService::class
        );
    */

    // Register extended controller class -> not needed yet because still uses default backend templates
    /*
    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
        ->registerImplementation(
            In2code\Femanager\Controller\UserBackendController::class,
            \HIVE\HiveOvrFemanager\Controller\UserBackendController::class
        );
    */

    // Hook for changing output before showing it
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-output'][]
        = \HIVE\HiveOvrFemanager\Hooks\ContentPostProc::class . '->run';

    // Tasks
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['HIVE\\HiveOvrFemanager\\Task\\SendConfirmationTask'] = [
        'extension' => 'hive_ovr_femanager',
        'title' => 'LLL:EXT:hive_ovr_femanager/Resources/Private/Language/locallang.xlf:tx_hiveovrfemanager_domain_model_user.sendConfirmationTask.title',
        'description' => 'LLL:EXT:hive_ovr_femanager/Resources/Private/Language/locallang.xlf:tx_hiveovrfemanager_domain_model_user.sendConfirmationTask.description',
    ];
});